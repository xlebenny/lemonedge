using System.Text.Json;

public static class Helper
{
    public static string ToJsonString(object obj)
    {
        var json = JsonSerializer.Serialize(obj);
        return json;
    }

    public static V GetOrDefault<K, V>(IDictionary<K, V> dict, K key, V defaultValue = default(V))
        where V : class
    {
        V value = defaultValue;
        return dict.TryGetValue(key, out defaultValue) ? value : defaultValue;
    }

    public static bool IsExist<K>(HashSet<K> set, K key)
        where K : class
    {
        K value;
        return set.TryGetValue(key, out value);
    }
}

public static class HelperExtension
{
    public static string ToJsonString(this object obj)
    {
        return Helper.ToJsonString(obj);
    }

    public static V GetOrDefault<K, V>(this IDictionary<K, V> dict, K key, V defaultValue = default(V))
        where V : class
    {
        return Helper.GetOrDefault(dict, key, defaultValue);
    }


    public static bool IsExist<K>(this HashSet<K> set, K key)
        where K : class
    {
        return Helper.IsExist(set, key);
    }
}