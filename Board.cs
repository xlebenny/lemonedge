using System.Collections.Immutable;

public abstract class IBoard
{
    public abstract string Display();
}

public interface IBoard_HorizontalRookCheck
{
    bool IsValid_IBoard_HorizontalRookCheck(string beforeUserInput, string userInput, string afterUserInput);
}

public interface IBoard_VerticalRookCheck
{
    bool IsValid_IBoard_VerticalRookCheck(string beforeUserInput, string userInput, string afterUserInput);
}

public class IBoard_Dummy : IBoard
{
    public IBoard_Dummy() { }

    public override string Display() => "Dummy Board";
}

public class Board_NumberPad : IBoard, IBoard_HorizontalRookCheck, IBoard_VerticalRookCheck
{
    protected static readonly string HardCodedLayout = ""
        + "+---+---+---+" + Environment.NewLine
        + "| 1 | 2 | 3 |" + Environment.NewLine
        + "+---+---+---+" + Environment.NewLine
        + "| 4 | 5 | 6 |" + Environment.NewLine
        + "+---+---+---+" + Environment.NewLine
        + "| 7 | 8 | 9 |" + Environment.NewLine
        + "+---+---+---+" + Environment.NewLine
        + "| * | 0 | # |" + Environment.NewLine
        + "+---+---+---+" + Environment.NewLine
        + "";

    protected static readonly Dictionary<string, HashSet<string>> HorizontalCheck = new Dictionary<string, HashSet<string>>
    {
        {
            "",
            new HashSet<string>{
            "1","2","3",
            "4","5","6",
            "7","8","9",
            "*","0","#"
            }
        },
        //
        { "1",  new HashSet<string>{"1","2","3"} },
        { "2",  new HashSet<string>{"1","2","3"} },
        { "3",  new HashSet<string>{"1","2","3"} },
        //
        { "4",  new HashSet<string>{"4","5","6"} },
        { "5",  new HashSet<string>{"4","5","6"} },
        { "6",  new HashSet<string>{"4","5","6"} },
        //
        { "7",  new HashSet<string>{"7","8","9"} },
        { "8",  new HashSet<string>{"7","8","9"} },
        { "9",  new HashSet<string>{"7","8","9"} },
        //
        { "*",  new HashSet<string>{"*","0","#"} },
        { "0",  new HashSet<string>{"*","0","#"} },
        { "#",  new HashSet<string>{"*","0","#"} },
    };

    protected static readonly Dictionary<string, HashSet<string>> VerticalCheck = new Dictionary<string, HashSet<string>>
    {
        {
            "",
            new HashSet<string>{
            "1","2","3",
            "4","5","6",
            "7","8","9",
            "*","0","#"
            }
        },
        //
        { "1",  new HashSet<string>{"1","4","7","*"} },
        { "4",  new HashSet<string>{"1","4","7","*"} },
        { "7",  new HashSet<string>{"1","4","7","*"} },
        { "*",  new HashSet<string>{"1","4","7","*"} },
        //
        { "2",  new HashSet<string>{"2","5","8","0"} },
        { "5",  new HashSet<string>{"2","5","8","0"} },
        { "8",  new HashSet<string>{"2","5","8","0"} },
        { "0",  new HashSet<string>{"2","5","8","0"} },
        //
        { "3",  new HashSet<string>{"3","6","9","#"} },
        { "6",  new HashSet<string>{"3","6","9","#"} },
        { "9",  new HashSet<string>{"3","6","9","#"} },
        { "#",  new HashSet<string>{"3","6","9","#"} },
    };

    public Board_NumberPad() { }

    // maybe i will change my mind later?
    public override string Display() => HardCodedLayout;

    public bool IsValid_IBoard_HorizontalRookCheck(string beforeUserInput, string userInput, string afterUserInput)
        => CheckRook(beforeUserInput, userInput, afterUserInput, HorizontalCheck);

    public bool IsValid_IBoard_VerticalRookCheck(string beforeUserInput, string userInput, string afterUserInput)
        => CheckRook(beforeUserInput, userInput, afterUserInput, VerticalCheck);


    private bool CheckRook(
        string beforeUserInput, string userInput, string afterUserInput,
        Dictionary<string, HashSet<string>> checkingLogic
    )
    {
        var characters = new List<string>();
        //
        var lastCharacter = (beforeUserInput.Length == 0) ? "" : beforeUserInput.Last().ToString();
        characters.Add(lastCharacter);
        //
        // i hate CHAR
        var userInputCharacters = userInput.ToCharArray().Select(x => x.ToString());
        characters.AddRange(userInputCharacters);
        //
        // if can't check
        if (characters.Count < 2) return true;
        //
        var current = characters.First();
        foreach (var next in characters.Skip(1))
        {
            var values = checkingLogic.GetValueOrDefault(current) ?? new HashSet<string> { };
            var isExist = values.IsExist(next);
            if (false == isExist) return false;
            // update
            current = next;
        }
        return true;
    }
}