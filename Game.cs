public class Game
{
    public readonly IBoard Board = new IBoard_Dummy();

    // yep, i know it still can add / remove item inside the list
    public readonly IValidationRule ValidationRule = new ValidationRule_Dummy();

    public readonly IEndGameRule EndGameRule = new EndGameRule_Dummy();

    public string InputedText { get; protected set; } = "";

    public Game(
        IBoard board, IValidationRule validationRule,
        IEndGameRule endGameRule
    )
    {
        this.Board = board ?? Board;
        this.ValidationRule = validationRule ?? ValidationRule;
        this.EndGameRule = endGameRule ?? EndGameRule;
    }

    public void Run()
    {
        Console.Clear();
        //
        Console.WriteLine($"Game Config:");
        Console.WriteLine($"  THE Rule:");
        // FIXME padding problem
        Console.WriteLine($"    {ValidationRule.Description}");
        Console.WriteLine($"  End game Rule:");
        // FIXME padding problem
        Console.WriteLine($"    {EndGameRule.Description}");
        Console.WriteLine($"");
        Console.WriteLine($"");
        Console.WriteLine($"Let start:");

        var isContinue = true;
        while (isContinue)
        {
            Console.WriteLine($"==========================================");
            Console.WriteLine(Board.Display());
            Console.WriteLine($"Current Input Text: {(InputedText == "" ? "<None>" : InputedText)}");
            Console.WriteLine($"Please input any character shown on above:");
            var userInput = Console.ReadLine() ?? "";
            Console.Clear();
            //
            var matchedViolatedRules = ValidationRule.CheckViolatedRules(InputedText, userInput, InputedText + userInput);
            var matchedEndGameRules = EndGameRule.CheckEndGameRules(InputedText, userInput, InputedText + userInput);
            //
            Console.WriteLine($"You inputed {InputedText + userInput}");
            if (matchedViolatedRules.Count > 0)
            {
                Console.WriteLine($"But your inputed violated:");
                // FIXME padding problem
                matchedViolatedRules.ForEach(x => Console.WriteLine($"  {x.Description}"));
                Console.WriteLine($"Please try again");
            }
            else if (matchedEndGameRules.Count > 0)
            {
                Console.WriteLine($"Matched End Game Rules:");
                // FIXME padding problem
                matchedEndGameRules.ForEach(x => Console.WriteLine($"  {x.Description}"));
                Console.WriteLine($"Bye");
                isContinue = false;
            }
            else
            {
                Console.WriteLine("C" + matchedViolatedRules.Count);
                Console.WriteLine("D" + matchedEndGameRules.Count);
                Console.WriteLine("A" + InputedText);
                Console.WriteLine("B" + userInput);
                // update
                InputedText = InputedText + userInput;
            }
        }
    }
}