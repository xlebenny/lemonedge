
public abstract class IValidationRule
{
    public abstract string Description { get; }
    public virtual bool IsSkip(string beforeUserInput, string userInput, string afterUserInput) => false;
    public abstract List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput);
    public override string ToString() => Helper.ToJsonString(this);
}

public class ValidationRule_Dummy : IValidationRule
{
    public override string Description => "Dummy Rule, always true";
    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput) => new List<IValidationRule>();
}

public class ValidationRule_ForbiddenCharacter : IValidationRule
{
    public override string Description => $"Can't contain forbidden character {string.Join(" ", ForbiddenCharacters)}";
    //
    public readonly List<string> ForbiddenCharacters = new List<string>();
    //
    public ValidationRule_ForbiddenCharacter(List<string> forbiddenCharacters)
    {
        this.ForbiddenCharacters = forbiddenCharacters;
    }
    //
    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        var isFailed = ForbiddenCharacters.Any(x => afterUserInput.Contains(x));
        return isFailed
            ? new List<IValidationRule>() { this }
            : new List<IValidationRule>() { };
    }
}

public class ValidationRule_ForbiddenStartCharacter : IValidationRule
{
    public override string Description => $"Check contain forbidden character in first character {string.Join(" ", ForbiddenCharacters)}";
    //
    public readonly List<string> ForbiddenCharacters = new List<string>();
    //
    public ValidationRule_ForbiddenStartCharacter(List<string> forbiddenCharacters)
    {
        this.ForbiddenCharacters = forbiddenCharacters;
    }
    //
    public override bool IsSkip(string beforeUserInput, string userInput, string afterUserInput) => (beforeUserInput != "");

    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        var isFailed = ForbiddenCharacters.Any(x => afterUserInput.Contains(x));
        return isFailed
            ? new List<IValidationRule>() { this }
            : new List<IValidationRule>() { };
    }
}

public class ValidationRule_FailCount : IValidationRule
{
    public override string Description => ""
        + $"Fail count ("
        + string.Join(Environment.NewLine, Rules.Select(x => $"  {x.Description},"))
        + $") >= {AcceptableFailCount}"
        + "";
    //
    public readonly List<IValidationRule> Rules = new List<IValidationRule>();
    public readonly long AcceptableFailCount = 0;
    //
    public ValidationRule_FailCount(List<IValidationRule> rules, long? acceptableFailCount)
    {
        this.Rules = rules ?? Rules;
        this.AcceptableFailCount = acceptableFailCount ?? AcceptableFailCount;
    }
    //
    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        var rules = Rules
            .Where(x => x.IsSkip(beforeUserInput, userInput, afterUserInput))
            .ToList();
        if (rules.Count == 0) return new List<IValidationRule>() { };
        //
        var notValidRules = new List<IValidationRule>() { };
        var thisLevelNotValidCount = 0;
        foreach (var rule in rules)
        {
            var tempNotValidRules = rule.CheckViolatedRules(beforeUserInput, userInput, afterUserInput);
            if (tempNotValidRules.Count > 0) thisLevelNotValidCount = thisLevelNotValidCount + 1;
            notValidRules.AddRange(tempNotValidRules);
        }
        // TODO maybe have a detail list with FAIL, WARN?
        var isFailed = (
            thisLevelNotValidCount >= AcceptableFailCount
            // e.g: accept 3 fail, but actual skiped 1, total checked 2, and 2 failed
            || (rules.Count < AcceptableFailCount && thisLevelNotValidCount == rules.Count)
        );
        return isFailed
            ? notValidRules
            : new List<IValidationRule>() { };
    }
}

public class ValidationRule_All : IValidationRule
{
    public override string Description => ""
        + $"All ("
        + string.Join(Environment.NewLine, Rules.Select(x => $"  {x.Description},"))
        + $")"
        + "";
    //
    public readonly List<IValidationRule> Rules = new List<IValidationRule>();
    //
    public ValidationRule_All(List<IValidationRule> rules)
    {
        this.Rules = rules ?? Rules;
    }
    //
    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        // HACK :)
        var rule = new ValidationRule_FailCount(Rules, Rules.Count);
        return rule.CheckViolatedRules(beforeUserInput, userInput, afterUserInput);
    }
}

public class ValidationRule_Any : IValidationRule
{
    public override string Description => ""
        + $"Any ("
        + string.Join(Environment.NewLine, Rules.Select(x => $"  {x.Description},"))
        + $")"
        + "";
    //
    public readonly List<IValidationRule> Rules = new List<IValidationRule>();
    //
    public ValidationRule_Any(List<IValidationRule> rules)
    {
        this.Rules = rules ?? Rules;
    }
    //
    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        // HACK :)
        var rule = new ValidationRule_FailCount(Rules, 1);
        return rule.CheckViolatedRules(beforeUserInput, userInput, afterUserInput);
    }
}


public class ValidationRule_HorizontalRookCheck : IValidationRule
{
    public override string Description => "It's horizontal align in the board between each press";
    //
    public readonly IBoard_HorizontalRookCheck Board;
    //
    public ValidationRule_HorizontalRookCheck(IBoard_HorizontalRookCheck board)
    {
        this.Board = board;
    }
    //
    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput)
        => Board.IsValid_IBoard_HorizontalRookCheck(beforeUserInput, userInput, afterUserInput)
            ? new List<IValidationRule>() { }
            : new List<IValidationRule>() { this };
}

public class ValidationRule_VerticalRookCheck : IValidationRule
{
    public override string Description => "It's vertical align in the board between each press";
    //
    public readonly IBoard_VerticalRookCheck Board;
    //
    public ValidationRule_VerticalRookCheck(IBoard_VerticalRookCheck board)
    {
        this.Board = board;
    }
    //
    public override List<IValidationRule> CheckViolatedRules(string beforeUserInput, string userInput, string afterUserInput)
        => Board.IsValid_IBoard_VerticalRookCheck(beforeUserInput, userInput, afterUserInput)
            ? new List<IValidationRule>() { }
            : new List<IValidationRule>() { this };
}