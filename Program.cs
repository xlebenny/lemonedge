﻿// See https://aka.ms/new-console-template for more information


global using System;
global using System.Collections.Generic;

var board = new Board_NumberPad();
var validationRule = new ValidationRule_Any(new List<IValidationRule>() {
        new ValidationRule_ForbiddenCharacter(new List<string>{"*", "#"}),
        new ValidationRule_ForbiddenStartCharacter(new List<string>{"0", "1"}),
        new ValidationRule_All(new List<IValidationRule>() {
            new ValidationRule_HorizontalRookCheck(board),
            new ValidationRule_VerticalRookCheck(board),
        }),
});
var endGameRule = new EndGameRule_Any(new List<IEndGameRule>(){
    new EndGameRule_LengthCheck(7),
});
var game = new Game(
    board: board,
    validationRule: validationRule,
    endGameRule: endGameRule
);
game.Run();