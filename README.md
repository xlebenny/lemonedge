# Readme

3 part

- Board.cs
  - Control how the board displayed, included chess piece
    (i think this case nonsense split into two class like Poker)
  - HACKED because readable

- ValidationRule.cs
  - First Part
    Check the patten like [poker](https://en.wikipedia.org/wiki/List_of_poker_hands)
    Some time need `Board` to help, because only `Board` know what is "Rook"
  - Second Part
    Similar to System.Linq.Expressions
    `AND(rule, rule2, rule3)`
    `OR(rule, rule2, rule3)`
    `rule(parameter1, parameter2)`

- EndGameRule.cs
  Same with `ValidationRule.cs`, but for how to ending the game
