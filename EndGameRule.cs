
public abstract class IEndGameRule
{
    public abstract string Description { get; }
    public virtual bool IsSkip(string beforeUserInput, string userInput, string afterUserInput) => false;
    public abstract List<IEndGameRule> CheckEndGameRules(string beforeUserInput, string userInput, string afterUserInput);
    public override string ToString() => Helper.ToJsonString(this);
}

public class EndGameRule_Dummy : IEndGameRule
{
    public override string Description => $"Dummy Rule, always true";

    public override List<IEndGameRule> CheckEndGameRules(string beforeUserInput, string userInput, string afterUserInput) => new List<IEndGameRule>() { };
}

public class EndGameRule_LengthCheck : IEndGameRule
{
    public override string Description => $"Total legnth should be >= {Length}";
    //
    public readonly long Length;
    //
    public EndGameRule_LengthCheck(long length)
    {
        this.Length = length;
    }
    // assume: null is haven't input yet
    public override List<IEndGameRule> CheckEndGameRules(string beforeUserInput, string userInput, string afterUserInput)
        => (afterUserInput?.Length < Length)
            ? new List<IEndGameRule>() { this }
            : new List<IEndGameRule>() { };
}



public class EndGameRule_FailCount : IEndGameRule
{
    public override string Description => ""
        + $"Fail count ("
        + string.Join(Environment.NewLine, Rules.Select(x => $"  {x.Description},"))
        + $") >= {AcceptableFailCount}"
        + "";
    //
    public readonly List<IEndGameRule> Rules = new List<IEndGameRule>();
    public readonly long AcceptableFailCount = 0;
    //
    public EndGameRule_FailCount(List<IEndGameRule> rules, long? acceptableFailCount)
    {
        this.Rules = rules ?? Rules;
        this.AcceptableFailCount = acceptableFailCount ?? AcceptableFailCount;
    }
    //
    public override List<IEndGameRule> CheckEndGameRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        var rules = Rules
            .Where(x => x.IsSkip(beforeUserInput, userInput, afterUserInput))
            .ToList();
        if (rules.Count == 0) return new List<IEndGameRule>() { };
        //
        var notValidRules = new List<IEndGameRule>() { };
        var thisLevelNotValidCount = 0;
        foreach (var rule in rules)
        {
            var tempNotValidRules = rule.CheckEndGameRules(beforeUserInput, userInput, afterUserInput);
            if (tempNotValidRules.Count > 0) thisLevelNotValidCount = thisLevelNotValidCount + 1;
            notValidRules.AddRange(tempNotValidRules);
        }
        // TODO maybe have a detail list with FAIL, WARN?
        var isFailed = (
            thisLevelNotValidCount >= AcceptableFailCount
            // e.g: accept 3 fail, but actual skiped 1, total checked 2, and 2 failed
            || (rules.Count < AcceptableFailCount && thisLevelNotValidCount == rules.Count)
        );
        return isFailed
            ? notValidRules
            : new List<IEndGameRule>() { };
    }
}

public class EndGameRule_All : IEndGameRule
{
    public override string Description => ""
        + $"All ("
        + string.Join(Environment.NewLine, Rules.Select(x => $"  {x.Description},"))
        + $")"
        + "";
    //
    public readonly List<IEndGameRule> Rules = new List<IEndGameRule>();
    //
    public EndGameRule_All(List<IEndGameRule> rules)
    {
        this.Rules = rules ?? Rules;
    }
    //
    public override List<IEndGameRule> CheckEndGameRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        // HACK :)
        var rule = new EndGameRule_FailCount(Rules, Rules.Count);
        return rule.CheckEndGameRules(beforeUserInput, userInput, afterUserInput);
    }
}

public class EndGameRule_Any : IEndGameRule
{
    public override string Description => ""
        + $"All ("
        + string.Join(Environment.NewLine, Rules.Select(x => $"  {x.Description},"))
        + $")"
        + "";
    //
    public readonly List<IEndGameRule> Rules = new List<IEndGameRule>();
    //
    public EndGameRule_Any(List<IEndGameRule> rules)
    {
        this.Rules = rules ?? Rules;
    }
    //
    public override List<IEndGameRule> CheckEndGameRules(string beforeUserInput, string userInput, string afterUserInput)
    {
        // HACK :)
        var rule = new EndGameRule_FailCount(Rules, 1);
        return rule.CheckEndGameRules(beforeUserInput, userInput, afterUserInput);
    }
}